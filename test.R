library(ggplot2)
library(caTools)
library(dplyr)
library(lubridate)
library(tm)
library(ROSE)
library(corrplot)
library(rpart)
library(rpart.plot)
library(randomForest)
library(pROC)

##############################################
#Q1 - a

setwd("C:/������� �������/��� �/����� �/����� ��� ������ �����/New folder")
carInsurance.raw <- read.csv("carInsurance_train.csv", stringsAsFactors = FALSE)
carInsurance <- carInsurance.raw

#Q1 - b
str(carInsurance)
#install.packages('datetime')
library(datetime)

#Q2 - c
carInsurance <- carInsurance[,c("CarInsurance","Job","Age","Education","Default","HHInsurance","CarLoan","Communication","NoOfContacts")]
str(carInsurance)

######################################################
#Q2

summary(carInsurance)
str(carInsurance)

coercx <- function(x,by){
  if(x<=by) return(x)
  return(by)
}

coercx.2 <- function(x,by){
  if(x>=by) return(x)
  return(by)
}

#CarInsurance
table(carInsurance$CarInsurance)
carInsurance$CarInsurance <- as.factor(carInsurance$CarInsurance)

#Job
table(carInsurance$Job)
ggplot(carInsurance,aes(Job,fill=CarInsurance))+geom_bar()
ggplot(carInsurance,aes(Job,fill=CarInsurance))+geom_bar(position='fill')

change.null.Job <- function(x){
  if (is.na(x)){
    return('managment')
  }
  return (x)
}

carInsurance$Job <- sapply(carInsurance$Job, change.null.Job)
carInsurance$Job <- as.factor(carInsurance$Job)

#Age
ggplot(carInsurance,aes(Age,fill=CarInsurance))+geom_histogram()
ggplot(carInsurance,aes(Age,fill=CarInsurance))+geom_histogram(position='fill')

carInsurance$Age <- sapply(carInsurance$Age,coercx,by=65 )
carInsurance$Age <- sapply(carInsurance$Age,coercx.2,by=22 )

#check Education
table(carInsurance$Education)
ggplot(carInsurance,aes(Education,fill=CarInsurance))+geom_bar()
ggplot(carInsurance,aes(Education,fill=CarInsurance))+geom_bar(position='fill')

change.null.Education <- function(x){
  if (is.na(x)){
    return('secondary')
  }
  return (x)
}

carInsurance$Education <- sapply(carInsurance$Education, change.null.Education)
carInsurance$Education <- as.factor(carInsurance$Education)


#Default
table(carInsurance$Default)
carInsurance$Default <- as.factor(carInsurance$Default )
ggplot(carInsurance,aes(Default,fill=CarInsurance))+geom_bar(position='fill')


#HHInsurance 
carInsurance$HHInsurance <- as.factor(carInsurance$HHInsurance )
ggplot(carInsurance,aes(HHInsurance,fill=CarInsurance))+geom_bar()
ggplot(carInsurance,aes(HHInsurance,fill=CarInsurance))+geom_bar(position='fill')

#CarLoan
carInsurance$CarLoan <- as.factor(carInsurance$CarLoan )
ggplot(carInsurance,aes(CarLoan,fill=CarInsurance))+geom_bar()
ggplot(carInsurance,aes(CarLoan,fill=CarInsurance))+geom_bar(position='fill')

#Communication 
# chane null to the mjority class
table(carInsurance$Communication)
ggplot(carInsurance,aes(Communication,fill=CarInsurance))+geom_bar()
ggplot(carInsurance,aes(Communication,fill=CarInsurance))+geom_bar(position='fill')

change.null.Communication <- function(x){
  if (is.na(x)){
    return('cellular')
  }
  return (x)
}

carInsurance$Communication <- sapply(carInsurance$Communication, change.null.Communication)
carInsurance$Communication <- as.factor(carInsurance$Communication)

#NoOfContacts
table(carInsurance$NoOfContacts)
ggplot(carInsurance,aes(NoOfContacts,fill=CarInsurance))+geom_bar()
ggplot(carInsurance,aes(NoOfContacts,fill=CarInsurance))+geom_bar(position='fill')

carInsurance$NoOfContacts <- sapply(carInsurance$NoOfContacts,coercx,by=5 )

str(carInsurance)

#we want that the prediction will be with probability so 
#we will change the type of the target to logical
carInsurance$CarInsurance <- ifelse(carInsurance$CarInsurance == 1, TRUE, FALSE)
as.logical(carInsurance$CarInsurance)

# traning and test set
filter <- sample.split(carInsurance$CarInsurance , SplitRatio = 0.7)

carInsurance.train <- subset(carInsurance, filter == T)
carInsurance.test <- subset(carInsurance, filter == F)

#######################################################################3
#Q3 - a
model.dt <- rpart(CarInsurance ~ .,carInsurance.train)
rpart.plot(model.dt, box.palette = "RdBu", shadow.col = "gray", nn = TRUE)

#Q3 - c

model.rf  <- randomForest(CarInsurance ~ .,carInsurance.train, importance = TRUE)

prediction.rf <- predict(model.rf ,carInsurance.test)
actual <- carInsurance.test$CarInsurance

# we intresting in the minority class
ggplot(carInsurance,aes(CarInsurance))+geom_bar()

cf.rf <- table(actual,prediction.rf>0.6)
precision.rf <- cf.rf[2,2]/(cf.rf[1,2]+ cf.rf[2,2]) #0.75
recall.rf <- cf.rf[2,2]/(cf.rf[2,1]+ cf.rf[2,2]) #0.19

#Q3 - d
prediction.dt <- predict(model.dt,carInsurance.test)

rocCurveDC <- roc(actual, prediction.dt, direction = ">", levels = c("TRUE","FALSE") )
rocCurveRF <- roc(actual, prediction.rf, direction = ">", levels = c("TRUE","FALSE") )

plot(rocCurveDC, col='red',main = "Roc Chart")
par(new = TRUE)
plot(rocCurveRF, col = 'blue', main = "ROC Chart")

auc(rocCurveDC)
auc(rocCurveRF)

#######################################################
#Q4 - a

profit_before <-200*(dim(carInsurance[carInsurance$CarInsurance=='TRUE',])[1]) - 100*dim(carInsurance)[1] #-79200

profit_after <- 200*(cf.rf[2,2]) - 100*((cf.rf[1,2])+(cf.rf[2,2])) #6400

precent <- (cf.rf[2,2]+cf.rf[1,2])/dim(carInsurance)[1] #0.04



